﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

/* Documentation
 * 
 * This code control the model animation and sound fx
 */

public class RDF_ObjectController : MonoBehaviour
{
    private Animator myAnimator;
    private Sequence animTweenSeq;

    private void Awake()
    {
        myAnimator = GetComponent<Animator>();
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        StartCoroutine(RandomActionGenerator());
    }

    private void OnDisable()
    {
        StopCoroutine(RandomActionGenerator());
    }

    IEnumerator RandomActionGenerator()
    {
        while (enabled)
        {
            yield return new WaitForSeconds(.75f);
            myAnimator.SetFloat("State", Random.Range(0f, 1f));
        }
    }

    void PlaySFX(string fxType)
    {
        if (fxType == "roar")
        {
            RDF_Audio_Manager.instance.PlayRoarFX();
        }
        else
        {
            if (Random.Range(0f, 1f) <= 0.5f)
                RDF_Audio_Manager.instance.PlayIdleFX();
        }
    }
}
