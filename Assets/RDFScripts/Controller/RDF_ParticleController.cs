﻿/* Documentation
 * 
 * This code inherit from CFX_AutoDestructShuriken by Jean Moreno,
 * link to unity JMO asset store: https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565
 * it wait for particle sfx finish emiting, and automatically deactivate the particle game object instead of destroying it for future use
 */

public class RDF_ParticleController : CFX_AutoDestructShuriken
{
    public PlayAudioType PlayingAudioType = PlayAudioType.None;

    protected override void OnEnable()
    {
        StartCoroutine(CheckIfAlive());
        if (PlayingAudioType == PlayAudioType.SpawnFX)
            RDF_Audio_Manager.instance.PlaySpawnFx();
        else if (PlayingAudioType == PlayAudioType.DespawnFx)
            RDF_Audio_Manager.instance.PlayDespawnFx();
    }
}

public enum PlayAudioType
{
    None,
    SpawnFX,
    DespawnFx
}