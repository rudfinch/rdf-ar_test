﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/* Documentation
 * 
 * This code handles the main menu (opening scene). Handle UI animations and UI interactions
 */

public class RDF_MainMenu : MonoBehaviour
{
    public RectTransform Title;
    public Image BG;
    public Text infoTxt;

    private bool loadScene = false;
    private bool startAR = false;

    private void Start()
    {
        RDF_Audio_Manager.instance.PlayMusic();
        StartCoroutine(OpeningAnimation());
    }

    private IEnumerator OpeningAnimation()
    {
        yield return new WaitForSeconds(0.8f);
        Title.DOAnchorPosY(-100, 1f).OnComplete(() =>
        {
            BG.DOFade(0.65f, 0.8f).OnComplete(() => BG.GetComponent<Button>().interactable = true);
            infoTxt.DOFade(1f, 0.8f);
        }
        );
    }

    public void StartApp()
    {
        startAR = true;
    }

    void Update()
    {
        if (startAR && loadScene == false)
        {
            loadScene = true;
            infoTxt.text = "Loading...";
            StartCoroutine(LoadNewScene());

        }

        if (loadScene == true)
        {
            infoTxt.color = new Color(infoTxt.color.r, infoTxt.color.g, infoTxt.color.b, Mathf.PingPong(Time.time, 1));
        }

    }

    IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(3);
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(2);
        while (!async.isDone)
        {
            yield return null;
        }

    }
}
