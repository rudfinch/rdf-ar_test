﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

/* Documentation
 * 
 * This is the in game/app main UI manager. It handles UI show & hide event, UI animation and user interaction(button click)
 */

public class RDF_UI_Manager : MonoBehaviour
{
    //public delegate void ArrowClickAction(bool increment);
    //public static event ArrowClickAction ArrowBtnClicked;

    public RectTransform ObjTitle, NextBtn, PrevBtn, InfoBtnContainer;
    public Text TitleTxt, InfoTxt;
    public RDF_InfoBox InfoBoxContainer;

    private RDF_Tracked_ObjManager _myInitiator;

    private void Start()
    {
        RDF_Audio_Manager.instance.PlayMusic();
    }

    #region Public_Method

    public void OpenTheUI(RDF_Tracked_ObjManager _initiator)
    {
        _myInitiator = _initiator;

        RefreshingUI();

        ObjTitle.DOAnchorPosY(-50f, 0.5f);
        InfoBtnContainer.DOAnchorPos(new Vector2(0f, -10f), 0.5f);
        NextBtn.DOAnchorPosX(-20f, 0.5f);
        PrevBtn.DOAnchorPosX(20f, 0.5f);
    }

    public void CloseTheUI()
    {
        ObjTitle.DOAnchorPosY(300f, 0.5f);
        InfoBtnContainer.DOAnchorPos(new Vector2(250f, -10f), 0.5f);
        NextBtn.DOAnchorPosX(200f, 0.5f);
        PrevBtn.DOAnchorPosX(-200f, 0.5f);

        if (InfoBoxContainer.gameObject.activeSelf)
            CloseInfoWindow();
    }

    public void RefreshingUI()
    {
        TitleTxt.text = _myInitiator.GetActiveObjTitle().Replace(";", "\n");
        InfoTxt.text = _myInitiator.GetActiveObjInfo().Replace(";", "\n");
    }

    private void OpenInfoWindow()
    {
        RDF_Audio_Manager.instance.PlayClickFX();
        InfoBoxContainer.gameObject.SetActive(true);
    }

    private void CloseInfoWindow()
    {
        InfoBoxContainer.CloseMe();
        //InfoBoxContainer.SendMessage("CloseMe", SendMessageOptions.DontRequireReceiver);
    }

    public void ArrowPressed(bool increase)
    {
        RDF_Audio_Manager.instance.PlayClickFX();
        _myInitiator.ChangeActiveMember(increase);
        //ArrowBtnClicked(increase);
        //_myInitiator.SendMessage("ChangeActiveMember", increase, SendMessageOptions.DontRequireReceiver);
    }

    public void GoBackToMain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    #endregion
}
