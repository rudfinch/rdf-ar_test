﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/* Documentation
 * 
 * This code handles the information dialog box,
 * a message box that appears when the user presses the info button that appears after the user scans the marker
 */

public class RDF_InfoBox : MonoBehaviour
{
    public Image Background;
    public RectTransform InfoWindow, ContentWindow, OpenInfo, CloseInfo;

    private Sequence openSeq;

    private void OnEnable()
    {
        OpenInfoWindow();
    }

    void OpenInfoWindow()
    {
        openSeq = DOTween.Sequence();

        openSeq.Append(Background.DOFade(0.65f, 0.5f));
        openSeq.Append(InfoWindow.DOScale(Vector3.one, 0.5f).OnComplete(() => ContentWindow.DOAnchorPosY(0f, 0f)));
        openSeq.Append(OpenInfo.DOAnchorPosX(250f, 0.25f));
        openSeq.Append(CloseInfo.DOAnchorPosX(0f, 0.25f));
        openSeq.Play();
    }

    public void CloseMe()
    {
        openSeq.Kill();
        openSeq = DOTween.Sequence();

        openSeq.Append(CloseInfo.DOAnchorPosX(250f, 0.2f));
        openSeq.Append(OpenInfo.DOAnchorPosX(0f, 0.2f));
        openSeq.Append(InfoWindow.DOScale(Vector3.zero, 0.5f));
        openSeq.Append(Background.DOFade(0f, 0.5f).OnComplete(() => gameObject.SetActive(false)));
        openSeq.Play();
    }
}
