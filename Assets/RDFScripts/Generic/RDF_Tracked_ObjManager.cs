﻿using System.Collections;
using UnityEngine;

/*
 * Documentation
 * 
 * This code responds to marker tracking, handles 3D Models & UI
 * This code must be placed on the child object of the RDF_Trackable_EventHandler game object, responding to the RDF_Trackable_EventHandler broadcast message
 */

public class RDF_Tracked_ObjManager : MonoBehaviour
{
    [SerializeField]
    RDF_UI_Manager _ui_Manager;

    [Header("Object Data Member")]
    public ObjectDatas[] ObjectDatas;

    public GameObject[] SpawnPart;
    public GameObject DespawnPart;

    [Header("Audio Datas")]
    public AudioClip Roar;
    public AudioClip[] idle;

    public GameObject[] shadowPlane;

    private int ActiveID = 0;
    private bool spawnNowActive = false;

    private void Awake()
    {
        foreach (GameObject obj in shadowPlane)
            obj.SetActive(false);
    }

    public string GetActiveObjTitle()
    {
        return ObjectDatas[ActiveID].Name + " [" + ObjectDatas[ActiveID].Element + "]";
    }

    public string GetActiveObjInfo()
    {
        return ObjectDatas[ActiveID].Info;
    }

    public void MarkerDetection(bool detected)
    {
        if (detected)
        {
            //foreach (RDF_ParticleController obj in this.SpawnPart)
            //obj.gameObject.SetActive(true);

            StartCoroutine(SpawnNow());
            _ui_Manager.OpenTheUI(this);
            //RDF_UI_Manager.ArrowBtnClicked += ChangeActiveMember;
            RDF_Audio_Manager.instance.ActiveObjMgr = this;
        }
        else
        {
            if (spawnNowActive)
            {
                spawnNowActive = false;
                StopCoroutine(SpawnNow());
                //foreach (RDF_ParticleController obj in this.SpawnPart)
                //obj.gameObject.SetActive(false);
            }
            if (ObjectDatas[ActiveID].ObjModel.activeSelf)
            {
                GameObject despawnFx = Instantiate(DespawnPart, transform.parent);
                despawnFx.transform.localPosition = Vector3.zero;
                despawnFx.GetComponent<RDF_ParticleController>().enabled = true;
            }
            RDF_Audio_Manager.instance.StopFX();
            ObjectDatas[ActiveID].ObjModel.SetActive(false);
            foreach (GameObject obj in shadowPlane)
                obj.SetActive(false);
            _ui_Manager.CloseTheUI();
            //RDF_UI_Manager.ArrowBtnClicked -= ChangeActiveMember;
        }
    }

    IEnumerator SpawnNow()
    {
        spawnNowActive = true;
        foreach (GameObject obj in SpawnPart)
        {
            GameObject spawnFx = Instantiate(obj, transform.parent);
            spawnFx.transform.localPosition = Vector3.zero;
            spawnFx.GetComponent<RDF_ParticleController>().enabled = true;
        }

        yield return new WaitForSeconds(0.8f);

        ObjectDatas[ActiveID].ObjModel.SetActive(spawnNowActive);
        foreach (GameObject obj in shadowPlane)
            obj.SetActive(spawnNowActive);

        spawnNowActive = false;
    }

    public void ChangeActiveMember(bool increase)
    {
        ObjectDatas[ActiveID].ObjModel.SetActive(false);

        if (increase)
        {
            if (ActiveID == ObjectDatas.Length - 1)
                ActiveID = 0;
            else
                ActiveID++;
        }
        else
        {
            if (ActiveID == 0)
                ActiveID = ObjectDatas.Length - 1;
            else
                ActiveID--;
        }

        _ui_Manager.RefreshingUI();
        ObjectDatas[ActiveID].ObjModel.SetActive(true);
    }

    private IEnumerator DespawnNow()
    {
        yield return new WaitForSeconds(0.8f);
    }
}

[System.Serializable]
public class ObjectDatas
{
    public string Name, Element, Info;
    public GameObject ObjModel;
}
