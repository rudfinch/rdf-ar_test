﻿using UnityEngine;
using Vuforia;

/*
 * Documentation
 * 
 * This code inherit from DefaultTrackableEventHandler, the purpose of this code is to modify the OnTrackingFound and OnTrackingLost event.
 * It broadcast to its children that has MarkerDetection method and passing bool variable
 */

public class RDF_Trackable_EventHandler : DefaultTrackableEventHandler //Inherit from DefaultTrackableEventHandler
{
    [SerializeField]
    RDF_Tracked_ObjManager trackedObjManager;

    #region OVERRIDE_METHODS

    protected override void OnTrackingFound()
    {
        trackedObjManager.MarkerDetection(true);
        //this.BroadcastMessage("MarkerDetection", true, SendMessageOptions.DontRequireReceiver);
    }


    protected override void OnTrackingLost()
    {
        //Checking previous tracking status to prevent un-necesary particle activation
        //if (this.m_PreviousStatus == TrackableBehaviour.Status.TRACKED)
        //{
        trackedObjManager.MarkerDetection(false);
        //this.BroadcastMessage("MarkerDetection", false, SendMessageOptions.DontRequireReceiver);
        //}
    }

    #endregion // OVERRIDE_METHODS
}
