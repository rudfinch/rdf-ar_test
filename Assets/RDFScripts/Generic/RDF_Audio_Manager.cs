﻿using UnityEngine;

/* Documentation
 * 
 * Master audio/audio manager.
 * Centralistic audio handler
 */

public class RDF_Audio_Manager : MonoBehaviour
{
    [Header("General Audio Datas")]
    public AudioClip clickClip;
    public AudioClip[] musics;
    public AudioClip SpawnFx, DespawnFx;

    public static RDF_Audio_Manager instance { get; private set; }

    public RDF_Tracked_ObjManager ActiveObjMgr { set; private get; }

    AudioSource music, sfx, otherFx;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            AudioSource[] sources = GetComponentsInChildren<AudioSource>();

            foreach (AudioSource source in sources)
            {
                if (source.name.Equals("Music"))
                {
                    music = source;
                }
                else if (source.name.Equals("MonsterFX"))
                {
                    sfx = source;
                }
                else
                {
                    otherFx = source;
                }
            }

            DontDestroyOnLoad(this);
        }
    }

    public void PlayMusic()
    {
        music.Stop();
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name.Equals("SampleScene"))
        {
            music.clip = musics[1];
        }
        else
        {
            music.clip = musics[0];
        }
        music.Play();
    }

    public void PlayRoarFX()
    {
        StopFX();
        if (ActiveObjMgr)
        {
            sfx.clip = ActiveObjMgr.Roar;
            sfx.Play();
        }
    }

    public void PlayIdleFX()
    {
        StopFX();
        if (ActiveObjMgr)
        {
            sfx.clip = ActiveObjMgr.idle[Random.Range(0, ActiveObjMgr.idle.Length)];
            sfx.Play();
        }
    }

    public void PlaySpawnFx()
    {
        otherFx.Stop();
        otherFx.clip = SpawnFx;
        otherFx.Play();
    }

    public void PlayDespawnFx()
    {
        otherFx.Stop();
        otherFx.clip = DespawnFx;
        otherFx.Play();
    }

    public void PlayClickFX()
    {
        otherFx.Stop();
        otherFx.clip = clickClip;
        otherFx.Play();
    }

    public void StopFX()
    {
        sfx.Stop();
    }

    public void StopMusic()
    {
        music.Stop();
    }
}
