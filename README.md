<b>About<b>

This is a Unity AR demo code that utilize integrated vuforia module.
Please print the ".png" file on [Root Directory]/Marker/Print-Me/ and use it as a marker


<b>Technical Info<b>

1. This demo made with Unity 2018.3.2f1 and it's integrated Vuforia module
2. This demo uses many free plug-in module from unity's assets store and the internet, they are:
    
    *  DoTween free version by Demigiant: https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676
    *  Free Dragon the Terror Bringer and Dragon Boar by Dungeon Mason: https://assetstore.unity.com/packages/3d/characters/creatures/dragon-the-terror-bringer-and-dragon-boar-77121
    *  Cartoon FX Free by JMO: https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565
    *  Endor & Immortal fonts from: https://www.1001freefonts.com/fantasy-fonts.php
    *  Graphical UI from: https://opengameart.org/content/fantasy-ui-elements-by-ravenmore

3. The audio on this demo are:
    *  Pegasus Fantasy: https://www.youtube.com/watch?v=f5R9uz-EuHY
    *  Escaflowne: https://www.youtube.com/watch?v=dHUR5AW27X0
    *  SFX from freesound.org like this click sound: https://freesound.org/people/Snapper4298/sounds/178186/, https://freesound.org/people/qubodup/sounds/442964/, https://freesound.org/people/JoelAudio/sounds/85568/, https://freesound.org/people/RICHERlandTV/sounds/216089/. etc


<b>Word from developer<b>

This code is free with GNU licence v3. 
This code might contain copyrighted sound fx and/or music, better use it privately. For public or commercial use, please remove or change the audio

I use 1080p resolution scaling for UI, you might need to change the UI to fit your needs (especially in portrait orientation mode for mobile devices).

Enjoy playing with this source code, please let me know if you found any bugs

Cheers,

Rudy